# Discuss With A Vampire

## Einleitung

Die Idee wurde im Winter 2023 geboren. _Winterfreizeit ist_ seit eh und jeh meine _Heimcomputer-Bastel-Zeit_.

Normalerweise restauriere ich dann _alten Elektroschrott_ und versuche das Ganze wieder wie am 1. Tag aussehen zu lassen. Leider aber derzeit nur entweder neue-, oder neuwertige Geräte in die Hände, sodass ich aktuell nichts zu restaurieren hatte.

Entsprechend habe ich mir dann _irgendwas mit Software_ gesucht. - Das ist eigentlich eh eher meine Domäne...

Dazu kam dann, dass ich mitbekommen habe, dass ich HNF in Paderborn 2024 wieder eine Ausstellung von Retro- (Heim-) Computern sein würde, an der ich, wie bereits in 2022, teilnehmen möchte.

2022 habe ich mich auf die Ausstellung meines ersten Heimcomputers, des Sinclair ZX Spectrum, sowie diverser Derivate des Gerätes konzentriert, dieses Jahr möchte ich die Ausstellung etwas anders angehen.

## Die Idee

Aktuell bastle ich gerade an einer Software herum, die als Schnittstelle zwischen dem Commodore Amiga, oder auch anderen alten Computern und dem PC fungiert. Mein Ziel ist es _möglichst lustige Features und Services_ abzubilden.

Eins davon ist eben ein möglichst interaktiver Chat zwischen dem User (_Hallo Tron!_)-, also dem Anwender des Amigas und dem alten Computer selbst.

Technische Details verrate ich aktuell noch nicht, ausser, dass der Amiga dazu eben zusätzlich mit der Außenwelt, in Form eines Linux Systems kommunizieren wird.

Das ganze soll interaktiv sein und in zwei Optionen auf Retro Events angeboten werden.

### Podiumsdiskussion mit einem Vampir

Am Beispiel eines Amiga 600 mit Vampire Turbokarte möchte ich als Moderator vor Publikum das Feature vorstellen.

Ziel-, wenn alles klappt, ist, dass ich eine "Podiumsdiskussion" moderiere, in der ich dem Rechner _Fragen stelle_, die er dann hoffentlich zur Zufriedenheit des Publikums beantwortet.

Als Aufhänger werde ich Stichworte verwenden wie:

1. Was ist die Antwort auf die letzte Frage der Menschheit?
1. Was ist/sind...
   1. der Amiga?
   1. der ZX Spectrum Next?
   1. andere Retro Computer Fragen...
1. Ein paar Scherze über Paderborn dürfen auch nicht fehlen. - Ich sach nur: _"PB ist ca. 1/2 so gross, wie der Friedhof von NY, ..."_

Im Anschluss daran möchte ich dazu überleiten, Fragen des Publikums von meinem Amiga beantworten zu lassen.

Wenn das so klappt, wie ich mir das vorstelle, **wird das eine ziemliche Show**. Geht es schief, wird's ein Reinfall. ;->

### Ein freundlicher Chat mit meinem Amiga

In diesem Modus bekommen die Besucher die Möglichkeit, selbst mit dem Rechner zu chatten. Hier sollen sie herausfinden, wie gut mir mein kleines Tool gelungen ist.

Ziel für mich ist es, möglichst viele Rückmeldungen zu bekommen, ob/wie sich das Ganze anfühlt.

## Der Idealfall für's HNF

Wenn alles so klappt, wie ich mir das vorstelle, wird ein ZX Spectrum Next (vermutlich in Form eines n-GO), den Chat _hosten_. Dieser Rechner bietet sich an, da er als 'Coprozessor' einen RasPI Zero besitzt. Der RasPI sollte ausreichend stark sein, um das, was ich zeigen möchte, zu betreiben.

Charme des Ganzen: Wie oben geschrieben, war der ZX Spectrum mein erster Heimcomputer und ist, nach wie vor, mein Steckenpferd, wenn es um Retro Computing geht.

Klappt das nicht-, bzw. vermutlich auch bei den Vorab Tests, werde ich für die Rolle des Hosts einen PC mitbringen.

## Bitte in die (Retro-) Computer Szene

Da ich vermeiden möchte, dass das Ganze zu einem Reinfall wird, suche ich nach Gelegenheiten, den aktuellen Stand meiner _Spielerei_ in der freien Wildbahn auszutesten.

Hierzu möchte ich den aktuellen Stand meiner _Bastelei_ interaktiv vor Ort ausprobieren.

## Aktueller Status

Status: LÄUFT (s.u.)
![Amiga 600 (Furia) mit Terminal Programm](resources/amiga_furia_terminal.jpg)

Auch wenn es die Furie ist und nicht der Vampir. DAS sollte nun wirklich keinen Unterschied machen, oder?!

## Disclaimer/Legal Stuff

Es handelt sich hier um ein "Non Profit", Public Domain Privatprojekt, mit dem keinerlei Geld-, oder sonstiger Gewinn erzeugt werden wird.

Die Basis des Projektbildes habe ich mir im Netz gesucht. Stichworte waren "Vampire" und "Public Domain". Herausgekommen ist ein Bild von https://wallpapercave.com/vampires-wallpapers . 1000(+1) Dank dafür!

Sollte es nicht möglich sein, dieses Bild "legal und ohne Lizenzkosten" zu verwenden: Lasst es mich bitte wissen und _klagt mich deswegen nicht zu Tode_.

Ich werde es _so schnell, wie mir möglich_ ersetzen, sollte ich erfahren, dass ich gegen irgendwelche Urheberrechte verstoßen habe.
