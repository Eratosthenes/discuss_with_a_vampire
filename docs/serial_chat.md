# Serial Chat

Serial Chat is a terminal program that allows you to connect your classic Commodore `Amiga` computer to a modern `ZX Spectrum Next` over a serial cable. It should be possible to connect more than one Amiga to the `ZX Spectrum Next`.

```mermaid
graph LR

PC["ZX Spectrum Next"]
PC -- Serial 1 <--> Amiga1["Amiga 1"]
PC -- Serial 2 <--> Amiga2["Amiga 2"]  
PC -- Serial 3 <--> Amiga3["Amiga 3"]

classDef grey fill:#fec,stroke:#fff,stroke-width:2px; 

class PC,Amiga1,Amiga2,Amiga3 grey
```

Why not use Ethernet/TCP-IP? _Well, because it's expensive on the classic `Amiga` system... ;-)_

`Serial Chat` should run on any `Amiga` running a terminal program. For the moment what will be provided is tested only on `Kickstart 3.1` systems. In general any computer can be connected that supports RS232 and vt200 terminal emulation.

## Overview

This application provides a bidirectional serial communication bridge between the `Amiga` and `ZX Spectrum Next` and do funny things with it.

You can use it to transfer files, access the `Amiga` shell and apps from your `ZX Spectrum Next` keyboard, and more.

Serial Chat works by opening a serial port on your `ZX Spectrum Next` and connecting it to the `Amiga`'s serial port using a null modem cable. It implements the basics of a terminal on both sides.

## Features

- Send text back and forth between the `Amiga` and `ZX Spectrum Next`  
- Transfer files between systems using XModem
- Access the `Amiga` CLI and programs from your `ZX Spectrum Next` keyboard
- Supports popular `Amiga` terminal programs like ATerm
- Lightweight and easy to use terminal interface

## Requirements  

- `Amiga` 500, 600, ... with serial port
- `ZX Spectrum Next` running Windows, Linux or macOS with serial port
- Serial null modem cable

## Getting Started

1. Connect the null modem cable between the `Amiga` and `ZX Spectrum Next` serial ports
1. Launch Serial Chat on the `ZX Spectrum Next`
1. Open a terminal program on the `Amiga`
1. Configure baud rates to match on both sides (Default: 9600 bps)
1. Start typing to send text back and forth!

## Usage Tips

- Use XModem protocol for basic file transfers between `Amiga` and `ZX Spectrum Next`
- Launch ATerm or another terminal on the `Amiga` for more features
- Adjust baud rate as needed if you encounter connectivity issues

## References To Other Projects

Here a list of some other projects providing terminal emulation for the `Amiga` and Linux.

### Running a Terminal on the `ZX Spectrum Next`

The `ZX Spectrum Next` has a terminal emulator provided with the current distribution of the system. It's called `Terminex`. it has some restrictions but can run the chat directly on the system.

### For The `Amiga`

`ATerm` hits that sweet spot of being full-featured yet fast and lightweight. It remains popular with `Amiga` users because of its ease of use, adherence to `Amiga` UI conventions, and reliable terminal emulation. For a great serial terminal experience on vintage `Amiga` hardware, I highly recommend giving ATerm a try.

- `ATerm` - A popular public domain terminal program for `Amiga`. Supports Xmodem transfers, scrollback buffer, and multiple windows.
  http://www.lysator.liu.se/~per_ake/at/

- `AmTelnet` - Shareware `Amiga` terminal for Telnet connections. The serial mode is free to try.
  http://aminet.net/package/comm/tcp/AmTelnet  

- `Caterpillar` - A simple serial terminal with Xmodem support. Small size and fast performance.
  http://www.lysator.liu.se/~aba/caterpillar.html

- `Minicom` - Open source terminal program ported to `Amiga`OS. Runs in CLI only but is quite configurable.
  http://aminet.net/package/comm/tcp/minicom  

- `SilverSurfer` - Shareware terminal program, but serial mode is free. More advanced features like Zmodem transfers.
  http://www.aaronpeeler.com/Silversurfer/

- `Termite` - Freeware terminal focused on SSH connections but has serial mode. Small and simple.
  http://aminet.net/package/comm/term/termite  

- `VT102` - Terminal emulator for `Amiga`DOS. Very lightweight with ANSI and VT102 emulation.
  http://aminet.net/package/term/term/vt102

### Linux Based Terminal Programs

For a good balance of power and ease of use, I'd recommend starting with `minicom` for CLI and `gtkterm` for a GUI app. But all of these options are solid serial terminal programs for Linux.

#### Linux CLI

- `cu` - Old school but simple serial terminal built into UNIX. No frills and just works.

- `minicom` - A popular text-based serial terminal for Linux. Very customizable and scriptable.

- `picocom` - A simple minimal terminal app. Easy to use with basic features.

- `screen` - A terminal multiplexer that can connect to serial ports. Very powerful and flexible.

#### Linux GUI

- `CuteCom` - Straightforward GUI app for serial terminals on Linux. Clean and easy to use.

- `gtkterm` - Open source terminal with tabs, logging, and port management.

- `Hterm` - Terminal emulator that can connect to serial ports. Built on web technologies.

- `qserialterm` - Basic but functional Qt-based serial terminal with some useful features. 

- `Serial Studio` - More full-featured commercial program with data visualization tools.
