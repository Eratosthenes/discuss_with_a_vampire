# Discuss with a Vampire-, the Furia or the ZX Spectrum Next himself

This environment allows you to talk to three of my favorite home computers
of the 80th and the early 90th.

To do so just select the alias of one of these and type in your questions or
suggestions.

The alias is one of these:

1. "Vampire" An Amiga 600 running a Vampire 2 accellerator card
2. "Furia"   Also an Amiga 600. This time powered up by a Furia 020
3. "ZX"      The ZX Spectrum next himself

Example: Vampire why should I visit the HNF in Paderborn?

To type the alias you can just type in the capitalized first letter followed
by the TAB key. E.g. "F" + TAB for Furia. Pressing the TAB key should auto
complete the name.

So give it a try and have a lot of fun.

MfL der Eratosthenes
