#!/usr/bin/env python3

import sys
import textwrap
from openai import OpenAI

umlaut_mappings = {
    'ä': 'ae', 'ö': 'oe', 'ü': 'ue', 
    'Ä': 'Ae', 'Ö': 'Oe', 'Ü': 'Ue',
    'ß': 'ss'
}

def escape_umlauts(text):
    for umlaut, replacement in umlaut_mappings.items():
        text = text.replace(umlaut, replacement)
    return text

def main():
    columns = 78

    command_line = ' '.join(sys.argv[1:])

    try:
        client = OpenAI()
        
        response = client.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "user", "content": command_line}]
        )
        
        text = escape_umlauts(response.choices[0].message.content.strip())
        print('\n'.join(textwrap.wrap(text, width=columns)))
        
    except Exception as e:
        print(f"Leider bin ich derzeit nicht besonders unterhaltsam. Grund:\r\n{e}")

if __name__ == "__main__":
    main()

