#!/bin/bash

all_arguments=""

for arg in "$@"; do
    # Überprüfe, ob die Variable bereits einen Wert enthält
    if [ -n "$all_arguments" ]; then
        all_arguments+=" "  # Füge ein Leerzeichen hinzu, wenn die Variable bereits einen Wert hat
    fi
    all_arguments+="$arg"  # Füge das aktuelle Argument zur Variable hinzu
done

request="$all_arguments"

# Use curl to get the answer to the question
curl_response=$(curl https://api.openai.com/v1/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
    "model": "gpt-3.5-turbo",
    "messages": [
      {
        "role": "user",
        "content": "'"${request}"'"
      }
    ]
  }' 2>/dev/null)

# Provide text
text=$(echo $curl_response |jq .choices[0].message.content)

# Replace umlauts
text_without_umlauts=$(echo $text | sed -e 's/ä/ae/g' -e 's/ö/oe/g' -e 's/ü/ue/g' -e 's/ß/ss/g' -e 's/Ä/Ae/g' -e 's/Ö/Oe/g' -e 's/Ü/Ue/g')

# Remove leading and ending quotation marks
text_without_umlauts="${text_without_umlauts#\"}"
text_without_umlauts="${text_without_umlauts%\"}"

echo ${text_without_umlauts//\"/} |fold -s -w78

