#!/usr/bin/env python3

from openai import OpenAI
client = OpenAI()

completion = client.chat.completions.create(
  model="gpt-3.5-turbo",
  messages=[
    {"role": "system", "content": "ZX Spectrum Next"},
    {"role": "user", "content": "Wie viele Menschen leben in Deutschland?"},
  ]
)

print(completion.choices[0].message.content)